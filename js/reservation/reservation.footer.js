
        Xbe.State.viewId = 110;
        Xbe.State.langId = 1;
        Xbe.State.hotelId = 528;
        Xbe.State.hotelGroupId = 507;
        Xbe.State.arrive = 635464224000000000;
        Xbe.State.depart = 635465088000000000;
        Xbe.State.rateCode = '';
        Xbe.State.roomCode = '';
        Xbe.State.shell = '006d24809c7b4c8a89cd6279bd6f129c';
        Xbe.State.packages = '';
        Xbe.State.template = 'Flex';
        Xbe.State.confirmNumber = '';
        Xbe.State.storedRezID = '00000000-0000-0000-0000-000000000000';
        Xbe.State.primaryChannelId = 1;
        Xbe.State.secondaryChannelId = 5;
        Xbe.State.transferRateGuid = '00000000-0000-0000-0000-000000000000';

        function LoginOnClick() {

            OpenModalFormFromInlineHtml('V110_C3_CustomerLoginPanel', 'V110_C3_cl_lblTitle1', 410, true, true);
        }

        var Page_ValidationActive = false;
        if (typeof(ValidatorOnLoad) == "function") {
            ValidatorOnLoad();
        }

        function ValidatorOnSubmit() {
            if (Page_ValidationActive) {
                return ValidatorCommonOnSubmit();
            } else {
                return true;
            }
        }

        if (_calendarArrivalInstance == null)
            _calendarArrivalInstance = GetCalendarLanguage('', 'en');

        jQuery("#V110_C1_ArrivalDatePicker").calendarsPicker({
            calendar: _calendarArrivalInstance,
            showOnFocus: true,
            showTrigger: '<img id="ButtonImageId_V110_C1_ArrivalDatePicker" src="CHAIN/507/images/shell/calendar_icon.gif" alt="Select Date" title="Select Date" >',
            isTripModify: false,
            isTripSummary: false,
            isCalendarDisplayed: true,
            isNightsDropdownDisplayed: false,
            renderer: jQuery.extend({}, jQuery.calendars.picker.themeRollerRenderer, {
                picker: jQuery.calendars.picker.themeRollerRenderer.picker.replace(/\{link:today\}/, '<span class="ui-datepicker-cmd-current">Arrival date:</span>')
            }),
            altField: '#V110_C1_ArrivalDatePicker_hiddenField',
            altFormat: 'DD, MM dd, YYYY',
            pickerClass: 'CalSelect',
            maxDate: _calendarArrivalInstance.newDate(2015, 9, 16),
            minDate: _calendarArrivalInstance.newDate(2014, 9, 16),
            defaultDate: _calendarArrivalInstance.newDate(2014, 9, 16),
            monthsToShow: 1,
            dateFormat: 'DD, MM dd, YYYY',
            changeMonth: false,
            prevText: '<span class="ui-icon ui-icon-circle-triangle-w"></span>',
            nextText: '<span class="ui-icon ui-icon-circle-triangle-e"></span>',
            prevStatus: 'Move one month backward',
            nextStatus: 'Move one month forward',
            onSelect: DatePickerArrivalOnSelect
        });
        jQuery(document).ready(function() {
            if (typeof(_arrivalDatePickerId) == 'undefined')
                _arrivalDatePickerId = '#V110_C1_ArrivalDatePicker';

            SetArrivalDatePickerValue(_calendarArrivalInstance.newDate(2014, 9, 16), '#V110_C1_ArrivalDatePicker');

        });

        if (_calendarDepartureInstance == null)
            _calendarDepartureInstance = GetCalendarLanguage('', 'en');

        jQuery("#V110_C1_DepartureDatePicker").calendarsPicker({
            calendar: _calendarDepartureInstance,
            showOnFocus: true,
            showTrigger: '<img id="ButtonImageId_V110_C1_DepartureDatePicker" src="CHAIN/507/images/shell/calendar_icon.gif" alt="Select Date" title="Select Date" >',
            isTripModify: false,
            isTripSummary: false,
            isCalendarDisplayed: true,
            isNightsDropdownDisplayed: false,
            renderer: jQuery.extend({}, jQuery.calendars.picker.themeRollerRenderer, {
                picker: jQuery.calendars.picker.themeRollerRenderer.picker.replace(/\{link:today\}/, '<span class="ui-datepicker-cmd-current">Departure date:</span>')
            }),
            altField: '#V110_C1_DepartureDatePicker_hiddenField',
            altFormat: 'DD, MM dd, YYYY',
            pickerClass: 'CalSelect',
            maxDate: _calendarDepartureInstance.newDate(2015, 9, 16),
            minDate: _calendarDepartureInstance.newDate(2014, 9, 16),
            defaultDate: _calendarDepartureInstance.newDate(2014, 9, 16),
            monthsToShow: 1,
            dateFormat: 'DD, MM dd, YYYY',
            changeMonth: false,
            prevText: '<span class="ui-icon ui-icon-circle-triangle-w"></span>',
            nextText: '<span class="ui-icon ui-icon-circle-triangle-e"></span>',
            prevStatus: 'Move one month backward',
            nextStatus: 'Move one month forward',
            onSelect: DatePickerDepartureOnSelect
        });
        jQuery(document).ready(function() {
            if (typeof(_departureDatePickerId) == 'undefined')
                _departureDatePickerId = '#V110_C1_DepartureDatePicker';

            SetDepartureDatePickerValue(_calendarDepartureInstance.newDate(2014, 9, 17), '#V110_C1_DepartureDatePicker');

        });
        if (_calendarInstance == null)
            _calendarInstance = GetCalendarLanguage('', 'en');

        _calendarMonths = 2;
        _calendarMaxNights = 31;
        _calendarHiddenButtonId = '#V110_C1_cc_HiddenAsyncButton';
        _calendarResetArrival = jQuery.calendars.newDate(2014, 9, 16);
        _calendarResetDeparture = jQuery.calendars.newDate(2014, 9, 17);
        jQuery(document).ready(function() {

            if (_CriteriaControlIds == null)
                SetCriteriaControlIds('V110_C1_QuantitiesCntrl_QuantityTable', 'V110_C1_NightsDropdown', 'V110_C1_CheckAvailFiltersControl_RateFiltersCntrl_RFDropDownList', 'V110_C1_OptionalFieldsCntrl_GrpTB', 'V110_C1_OptionalFieldsCntrl_PromoTB', 'V110_C1_OptionalFieldsCntrl_IataTB', 'V110_C1_CheckAvailFiltersControl_RoomTypeHiddenField', 'V110_C1_CheckAvailFiltersControl_RoomFeatureHiddenField', 'V110_C1_CheckAvailFiltersControl_RatesHiddenField', 'V110_C1_QuantitiesCntrl_RoomsRepeater_ctl00_ChildrenAgesHF', 'V110_C1_OptionalFieldsCntrl_InvalidGroupLabel', 'V110_C1_OptionalFieldsCntrl_InvalidPromoLabel', 'V110_C1_OptionalFieldsCntrl_InvalidIataLabel');
        });
        jQuery("#V110_C1_cc_CalendarAvailability").calendarsPicker({
            calendar: _calendarInstance,
            isHighlightWeekendsAndHolidays: false,
            weekEndDefinition: 0,
            renderer: jQuery.extend({}, jQuery.calendars.picker.themeRollerRenderer, {
                picker: jQuery.calendars.picker.themeRollerRenderer.picker.replace(/\{link:today\}/, '<span class="ui-datepicker-cmd-current">Select dates</span>')
            }),
            multiSelect: (_calendarMaxNights + 1),
            altField: '#V110_C1_cc_CalendarAvailability_hiddenField',
            altFormat: 'm/d/YYYY',
            onDate: SetCalendarDisplay,
            pickerClass: 'CalSelect',
            maxDate: _calendarInstance.newDate(2015, 9, 16),
            minDate: _calendarInstance.newDate(2014, 9, 16),
            defaultDate: _calendarInstance.newDate(2014, 9, 16),
            monthsToShow: [1, 2],
            monthsOffset: 1,
            dateFormat: 'm/d/YYYY',
            changeMonth: false,
            prevText: '<span class="ui-icon ui-icon-circle-triangle-w"></span>',
            nextText: '<span class="ui-icon ui-icon-circle-triangle-e"></span>',
            prevStatus: 'Move one month backward',
            nextStatus: 'Move one month forward',
            onSelect: AvailabilityCalendarOnSelect,
            onChangeMonthYear: AvailabilityCalendarOnChangeMonthYear
        });
        jQuery(document).ready(function() {
            _availabilityCalendarId = '#V110_C1_cc_CalendarAvailability';
            if (typeof(_arrivalDatePickerId) == 'undefined')
                _arrivalDatePickerId = '#V110_C1_ArrivalDatePicker';
            if (typeof(_departureDatePickerId) == 'undefined')
                _departureDatePickerId = '#V110_C1_DepartureDatePicker';
            _isCalendarSetOnLoad = true;
            _drawMonthsInDisplay = [9, 10];
            _minDate = _calendarInstance.newDate(2014, 9, 16);
            _legendClientId = '#V110_C1_cc_BECalLegend';
            _totalPriceLabelClientId = '';
            _totalPriceNumberRoomsLabelClientId = '';
            AvailabilityCalendarOnChangeMonthYear(2014, 9);
            SetAvailabilityCalendarDates(_availabilityCalendarId, _calendarInstance.newDate(2014, 9, 16), ValidateDepartureDate(_calendarInstance.newDate(2014, 9, 16), _calendarInstance.newDate(2014, 9, 17)));
            CriteriaSetControls(_calendarInstance.newDate(2014, 9, 16), ValidateDepartureDate(_calendarInstance.newDate(2014, 9, 16), _calendarInstance.newDate(2014, 9, 17)), false);
            _isOnChangeNummberOfNights = false;

        });

         //]]>
